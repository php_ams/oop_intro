<?php
  class User {
    private $name;
    private $age;

    // runs when an object is created
    public function __construct($name, $age) {
      $this->name = $name;
      $this->age = $age;
    }

    public function getName() {
      return $this->name;
    }

    public function setName($name) {
      $this->name = $name;
    }

    // __get MAGIC METHOD
    public function __get($property) {
      if (property_exists($this, $property)) {
        return $this->$property;
      }
    }

    // __set MAGIC METHOD
    public function __set($property, $value) {
      if (property_exists($this, $property)) {
        $this->$property = $value;;
      }
    }

    // called when no other references to a certain object
    // User for cleanup, closing connections etc.
    public function __destruct() {
      //echo 'destructor ran';
    }
  }

  $user1 = new User('Max', 30);
  // echo $user1->getName();
  $user1->__set('age', 31);
  echo $user1->__get('name');
