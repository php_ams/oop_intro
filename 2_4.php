<?php
  class User {
    public $name;
    public $age;

    // runs when an object is created
    public function __construct($name, $age) {
      $this->name = $name;
      $this->age = $age;
    }

    public function sayHello() {
      return $this->name . ' says Hello';
    }

    // called when no other references to a certain object
    // User for cleanup, closing connections etc.
    public function __destruct() {
      echo 'destructor ran';
    }
  }

  $user1 = new User('Max', 30);
